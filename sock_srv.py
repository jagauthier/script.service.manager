#!/usr/bin/python
import select
import socket
import sys

from collections import Counter

# Do not block forever (milliseconds)
TIMEOUT = 1000

# Commonly used flag sets
READ_ONLY = select.POLLIN | select.POLLPRI | select.POLLHUP | select.POLLERR
READ_WRITE = READ_ONLY | select.POLLOUT


class sock_srv:
    
    # variables
    # Keep up with the queues of outgoing messages
    poller=None
    server=None
    fd_to_socket=None
    end=False
    connection_callback=None
    
    def init(self, ip, port, log):
        # Create a TCP/IP socket
        sock_srv.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock_srv.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)        
        sock_srv.server.setblocking(0)
        
        # Bind the socket to the port
        server_address = (ip, port)

        if log==None:
            print >>sys.stderr, 'starting up on %s port %s' % server_address
        else:
            log("Starting on " + ip + ":"+str(port))

        sock_srv.server.bind(server_address)
        
        # Listen for incoming connections
        sock_srv.server.listen(5)
        
        # Set up the poller
        sock_srv.poller = select.poll()
        sock_srv.poller.register(sock_srv.server, READ_ONLY)

        # Map file descriptors to socket objects
        sock_srv.fd_to_socket = { sock_srv.server.fileno(): sock_srv.server,
        }

    # support callback functions for dealing with network responses and logging
    def serve_end(self):
        sock_srv.end=True
        
    # support callback functions for dealing with network responses and logging
    def set_conn_callback(self, func):
        self.connection_callback=func

    def serve(self, func,log):
        while sock_srv.end==False:
            # Wait for at least one of the sockets to be ready for processing
            events = sock_srv.poller.poll(TIMEOUT)
            for fd, flag in events:
                # Retrieve the actual socket from its file descriptor
                s = sock_srv.fd_to_socket[fd]
                # Handle inputs
                if flag & (select.POLLIN | select.POLLPRI):
			
                    if s is sock_srv.server:
                        # A "readable" server socket is ready to accept a connection
                        connection, client_address = s.accept()
                        if log==None:
                            print >>sys.stderr, 'new connection from', client_address
                        else:
                            log("New connection from " + client_address[0] + ":" + str(client_address[1]))
                        connection.setblocking(0)
                        sock_srv.fd_to_socket[ connection.fileno() ] = connection
                        sock_srv.poller.register(connection, READ_ONLY)
                        self.connection_callback(connection)
                    else:
                        try :
                            data = s.recv(1024)
                            if data:
                                # Add output channel for response
                                sock_srv.poller.modify(s, READ_WRITE)
                                # call back function to do whatever we want with the received data
                                func(s, data)
                            else:
                                # Interpret empty result as closed connection
                                if log==None:
                                    print >>sys.stderr, 'closing', client_address, 'after reading no data'
                                else:
                                    log("Disconnected " + client_address[0] + ":" + str(client_address[1]))
                                    # Stop listening for input on the connection
                                sock_srv.poller.unregister(s)
                                s.close()
                        except:
                            if log==None:
                                print >>sys.stderr, 'closing', client_address, 'after data exception'
                            else:
                                log("Exception " + client_address[0] + ":" + str(client_address[1]))
                                raise
                            sock_srv.poller.unregister(s)
                            s.close()                            

                elif flag & select.POLLOUT:
                    sock_srv.poller.modify(s, READ_ONLY)
                elif flag & select.POLLHUP:
                    # Client hung up
                    if log==None:
                        print >>sys.stderr, 'closing', client_address, 'after receiving HUP'
                    else:
                        log("Received HUP " + client_address[0] + ":" + str(client_address[1]))
                    # Stop listening for input on the connection
                    sock_srv.poller.unregister(s)
                    s.close()
                elif flag & select.POLLERR:
                    if log==None:
                        print >>sys.stderr, 'handling exceptional condition for', s.getpeername()
                    else:
                        log("handling exceptional condition for " +  s.getpeername())[0]
                    # Stop listening for input on the connection
                    sock_srv.poller.unregister(s)
                    s.close()
			
    def send_all(self, socket, data):
        for k, v in Counter(sock_srv.fd_to_socket).iteritems():
            # Do not send the data to the socket that triggered this, or th server socket
            if (v is not sock_srv.server) and (v is not socket):
                try:
                    v.send(data)
                except:
                    pass
            
    def valid_socket(self, socket):
        for k, v in Counter(sock_srv.fd_to_socket).iteritems():
            # Do not send the data to the socket that triggered this, or th server socket
            if (v is socket):
                return True
        return False