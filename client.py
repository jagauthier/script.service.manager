#!/usr/bin/python
import socket
import sys
import time
import threading

messages = [ 'get_kodi_prop radio_freq',
	'get_kodi_prop radio_callsign',
    'get_kodi_prop radio_pty',
    'get_kodi_prop radio_text',
    'get_kodi_prop radio_ps',]
server_address = ('localhost', 4703)

def recv():
    while True:
        data = sock.recv(1024)
        if not data:
            print >>sys.stderr, 'closing socket', sock.getsockname()
            sock.close()
        print "Received: " + data.rstrip()

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)        

# Create a TCP/IP socket


# Connect the socket to the port where the server is listening
print >>sys.stderr, 'connecting to %s port %s' % server_address
sock.connect(server_address)

threading.Thread(target=recv).start()

while True:
    for message in messages:
        print >>sys.stderr, '%s: sending "%s"' % (sock.getsockname(), message)
        sock.send(message+"\n")


