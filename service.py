#!/usr/bin/env python
import threading
import time
import signal
import sock_srv

try:
    # so we can run this outside of kodi
    import xbmc
    import xbmcgui
    import xbmcaddon
    no_kodi=False
except ImportError:
    no_kodi=True

TCP_IP="0.0.0.0"
# picked the chipset of the radio
TCP_PORT=4703
BUFFER_SIZE=256

net_thread=True

variables={}

def log(logline):
    print "MANAGER: " + logline

def set_kodi_prop(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)
    if no_kodi==True:
        variables[property]=value
    else:
        xbmcgui.Window(10000).setProperty(property, strvalue)

def get_kodi_prop(property):
    if no_kodi==True:
        if property in variables:
            value=variables[property]
        else:
            value=""
    else:
        value=xbmcgui.Window(10000).getProperty(property)
    ### Exception - because it's really annoying seeing once a second
    if property!='radio_active':
        log("Loading: '" + property + "', '" + value + "'")
    return value

def signal_handler(signal, frame):
    global net_thread
    net_thread=False

    
class Manager():
    
    #socket server 
    ss = None
    
    # The radio socket for sending radio commands. Determined and set upon radio connection
    radio_sock=None
    
    def __init__(self):
        # this will be used by other applications to determine if this specific service
        # it running inside kodi or outside
        if no_kodi==False:
            set_kodi_prop("kodi_manager", "1")
    
    def start(self):
        # A big part of this service is going to be handling network calls to connected clients
        # At the time of this writing it will mostly be the radio functions, but will be extended
        # to GPS, and car data if the performance is good, which I think it will be.
        
        # pass the log function        
        self.ss=sock_srv.sock_srv()
        self.ss.init(TCP_IP, TCP_PORT,log)
        self.ss.set_conn_callback(self.new_conn)
        self.ss.serve(self.network_response,log)
        
    def new_conn(self, socket):
        # Dump all the known variables to the client
        for k, v in variables.iteritems():
            socket.send("set_kodi_prop " + k + " " + v + "\n")
    
    def end(self):
        global net_thread
        log("Ending.")
        self.ss.serve_end()
        net_thread=False
        
    def network_response(self,socket,data):
        data_split=data.split("\n")
        
        # loop through the data
        for data_line in data_split:
            request=data_line.split(" ")
            if request[0]=="RADIO":
                if (self.radio_sock!=None):
                    if self.ss.valid_socket(self.radio_sock):
                        # send the remaining commands, but not "RADIO"
                        # if we've detected the radio is connected
                        self.radio_sock.send(" ".join(request[1:])+"\n")
                    else:
                        self.radio_sock=None
                    
                if request[1]=="SERVICE":
                    self.radio_sock=socket
            if request[0]=="TUNE":
                tune_str=request[1]
                tune=True
                    
            if request[0]=="get_kodi_prop":
                socket.send("set_kodi_prop " + request[1] + " " + get_kodi_prop(request[1])+"\n")
            if request[0]=="set_kodi_prop":
                set_kodi_prop(request[1], " ".join(request[2:]))
                    # tell all connected clients to update their local variables
                self.ss.send_all(socket, data)
            
def kodi_mon():
    global net_thread
    monitor = xbmc.Monitor()
    while True:
        if monitor.waitForAbort(1):
            Manager.end(manager)
            net_thread=False
            break

if __name__ == "__main__":
    global manager

    log("Starting up")
    
    if no_kodi==False:
        threading.Thread(target=kodi_mon).start()
    else:
        signal.signal(signal.SIGINT, signal_handler)

    ## Remove - testing only

    manager = Manager()
    manager.start()
